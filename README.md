## Exam - UI Automation

This automation exam is written using Cypress and run within a continuous integration pipeline in Bitbucket.

---

## About the tests

Application URL: https://www.demo.bnz.co.nz/client/

The following tests are automated:  
**TC1: Verify you can navigate to Payees page using the navigation menu**  
1. Click ‘Menu’  
2. Click ‘Payees’  
3. Verify Payees page is loaded

**TC2: Verify you can add new payee in the Payees page**  
1. Navigate to Payees page  
2. Click ‘Add’ button  
3. Enter the payee details (name, account number)  
4. Click ‘Add’ button  
5. ‘Payee added’ message is displayed, and payee is added in the list of payees

**TC3: Verify payee name is a required field**  
1. Navigate to Payees page  
2. Click ‘Add’ button  
3. Click ‘Add’ button  
4. Validate errors  
5. Populate mandatory fields  
6. Validate errors are gone

**TC4: Verify that payees can be sorted by name**  
1. Navigate to Payees page  
2. Add new payee  
3. Verify list is sorted in ascending order by default  
4. Click Name header  
5. Verify list is sorted in descending order  

**TC5: Navigate to Payments page**  
1. Navigate to Payments page  
2. Transfer $500 from Everyday account to Bills account  
3. Transfer successful message is displayed  
4. Verify the current balance of Everyday account and Bills account are correct  
Note: Run this test 3 times to ensure 100% pass rate  

---

## Set up - When you want to run the test locally (Windows)
**Install Cypress on Windows**  
1. Download and Install Node js and npm from the Node JS Official website. Both Node Js and Node package manager(npm) will be installed from a single installer file.  
2. Create a folder where you want to install Cypress (e.g. C:\Datacom\UI Test Automation\cypress-framework)  
3. Open Command Prompt and run the command  
    npm init  
4. Run the command  
    npm install cypress --save-dev  

That's it! You have successfully installed cypress!  

** Run your Cypress test**  
Your Cypress test should be under .\cypress\integration folder  
Run the test using this command: npm run cy:run --spec  ".\cypress\integration\{Test name}.js"  