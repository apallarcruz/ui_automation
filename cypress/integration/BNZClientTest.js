/// <reference types="Cypress" />

import Dashboard from "./PageObjects/Dashboard";
import Payees from "./PageObjects/Payees";
import PayOrTransfer from "./PageObjects/PayOrTransfer";


Cypress._.times(3, function(){
    describe('BNZ Client Tests', function(){
        
        it('TC 1: Navigate to Payees page using the navigation menu', function(){
            Dashboard.goToBNZClientWebsite();
            Payees.navigateToPayeesAndVerifyPageLoaded();
        })

        it('TC 2: Add new payee in the Payees page', function(){
            Payees.navigateToPayees();
            Payees.addPayee('Test Payee{enter}', '01', '1234', '5678999', '00{enter}');
        })

        it('TC 3: Verify payee name is a required field', function(){
            Payees.navigateToPayees();
            Payees.verifyPayeeNameIsRequired();
        })

        it('TC 4: Verify that payees can be sorted by name', function(){
            Payees.navigateToPayees();
            Payees.verifyListOfPayeesSortedInAscOrder();
            Payees.clickNameHeaderAndVerifyPayeesSortedInDescOrder();
        })

        it('TC 5: Transfer amount from Everyday acct to Bills acct', function(){

            PayOrTransfer.navigateToPayments();
            PayOrTransfer.transferMoneyFromEverydayToBills('500');
        })

    })
})