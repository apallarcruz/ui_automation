/// <reference types="Cypress" />

class Dashboard{

    static goToBNZClientWebsite()
    {
        cy.visit(Cypress.env('url'));
    }

    static clickMenu(){
        cy.get('.Icons--hamburguerMenu').click();
        cy.get('.MainMenu-bnzIcon').should('be.visible')
    }
}
export default Dashboard;