/// <reference types="Cypress" />

import { parseInt } from "lodash";

class PayOrTransfer{

//Locators
    //From Account
    static getFromAcctBtn(){
        cy.wait(10000);
        return cy.get('button[data-testid="from-account-chooser"]');
    }

    //To account
    static getToAcctBtn(){
        return cy.get('button[data-testid="to-account-chooser"]');
    }

    //search text field
    static getSearchTextBox(){
        return cy.get('input[placeholder="Search"]');
    }

    //get Account tab
    static getAccountsTab(){
        return cy.get('li[data-testid="to-account-accounts-tab"]');
    }

    //get Everyday
    static getEverydayAcct(){
        return cy.get('button').contains('Everyday');
    }

    //get Bills acct
    static getBillsAcct(){
        return cy.get('button').contains('Bills');
    }
    
    //Amount text field
    static getAmountTextBox(){
        return cy.get('input[name="amount"]');
    }

    //Pay Or Transfer Button
    static getPayOrTransferBtn(){
        return cy.get('button[data-monitoring-label="Transfer Form Submit"]');
    }

//////////////////////////-- Keywords -- //////////////////////////////////
    //Click From Acct
    static clickFromAcctBtn(){
        PayOrTransfer.getFromAcctBtn().click();
    }

    static clickToAcctBtn(){
        PayOrTransfer.getToAcctBtn().click();
    }

    //Input to Search field
    static selectEveryday(){
        PayOrTransfer.getEverydayAcct().click();
    }

    static selectBills(){
        PayOrTransfer.getBillsAcct().click();
    }
    
    static clickAccountsTab(){
        PayOrTransfer.getAccountsTab().click();
    }

    static inputAmount(amount){
        PayOrTransfer.getAmountTextBox().type(amount);
    }

    static clickPayOrTransferBtn(){
        PayOrTransfer.getPayOrTransferBtn().click();
    }

    static transferMoneyFromEverydayToBills(amount){
        PayOrTransfer.clickFromAcctBtn();
        PayOrTransfer.selectEveryday();
        PayOrTransfer.clickToAcctBtn();
        PayOrTransfer.clickAccountsTab();
        PayOrTransfer.selectBills();
        PayOrTransfer.inputAmount(amount);
        PayOrTransfer.clickPayOrTransferBtn();

        //Everyday Acct - Balance should be diminished 500
        cy.get('#account-ACC-1 span.account-balance').then($span => 
        {
            const everydayBalNew = parseInt($span.text().replace(/,/g, ""))
            expect(everydayBalNew - 500).to.eq(everydayBalNew - 500);
        });

        //Bills balance should be increased by 500
        cy.get('#account-ACC-5 span.account-balance').then($span => 
            {
                const everydayBalNew = parseInt($span.text().replace(/,/g, ""))
                expect(everydayBalNew + 500).to.eq(everydayBalNew + 500);
            });
    }

    static navigateToPayments(){
        cy.visit(Cypress.env('url')+"/payments");
    }
}

export default PayOrTransfer;