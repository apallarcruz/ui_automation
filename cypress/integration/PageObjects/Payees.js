/// <reference types="Cypress" />

import Dashboard from "./Dashboard";

class Payees{

// Locators
    //client/payees URL
    static getPayeesBtn(){
        return cy.get('a[href="/client/payees"]');
    }
    //Add Payee button
    static getAddPayeeBtn(){
        return cy.get('.js-add-payee');
    }
    //Payee Name textbox
    static getPayeeNameTextBox(){
        return cy.get('#ComboboxInput-apm-name');
    }
    //Bank acct input fields
    static getBankTextBox(){
        return cy.get('#apm-bank');
    }
    static getBranchTextBox(){
        return cy.get('#apm-branch');
    }
    static getAccountTextBox(){
        return cy.get('#apm-account');
    }
    static getSuffixTextBox()
    {
        return cy.get('#apm-suffix');
    }
    //Add button - Add payee
    static getSubmitPayeeBtn()
    {
        return cy.get('button.js-submit');
    }

    static getNameHeader(){
        return cy.get('.js-payee-name-column');
    }
// Keywords

    static navigateToPayeesAndVerifyPageLoaded()
    {
        cy.wait(5000);
        Dashboard.clickMenu();
        Payees.getPayeesBtn().click();
        cy.url().should('include', '/payees');
        cy.get('span:contains("Payees")').should('be.visible');
    }

    static clickAddPayee(){
        Payees.getAddPayeeBtn().click();
    }
    
    static verifyPayeeNameIsRequired()
    {
        Payees.clickAddPayee();
        Payees.submitPayeeDetails();
        Payees.verifyErrorMsgDisplays();
    }
    static addPayee(name, bank, branch, acct, suffix)
    {
        Payees.clickAddPayee();
        Payees.enterPayeeDetails(name, bank, branch, acct, suffix);
        Payees.submitPayeeDetails();
        Payees.verifyPayeeAddedMsg();
    }

    static enterPayeeDetails(name, bank, branch, acct, suffix)
    {
        Payees.getPayeeNameTextBox().type(name);
        Payees.getBankTextBox().type(bank);
        Payees.getBranchTextBox().type(branch);
        Payees.getAccountTextBox().type(acct);
        Payees.getSuffixTextBox().type(suffix);
    }

    static submitPayeeDetails()
    {
        Payees.getSubmitPayeeBtn().click();
    }

    static verifyPayeeAddedMsg()
    {
        cy.get('#notification').should('contain', 'Payee added');
    }

    static verifyErrorMsgDisplays()
    {
        cy.get('.error-header').should('contain', 'A problem was found. Please correct the field highlighted below.');
    }

    static navigateToPayees()
    {
        cy.visit(Cypress.env('url')+"/payees");
    }

    static verifyListOfPayeesSortedInAscOrder()
    {
        cy.wait(5000);
        cy.get('.List').then($elements =>
        {
            const strings = [...$elements].map(el => el.innerText)
            expect(strings).to.deep.equal([...strings].sort())
        });
    }

    static verifyListOfPayeesSortedInDescOrder()
    {
        cy.wait(5000);
        cy.get('.List').then($elements =>
        {
            const strings = [...$elements].map(el => el.innerText)
            expect(strings).to.not.equal([...strings].sort())
        });
    }

    static clickNameHeaderAndVerifyPayeesSortedInDescOrder()
    {
        Payees.getNameHeader().click();
        Payees.verifyListOfPayeesSortedInDescOrder();
    }

}
export default Payees;